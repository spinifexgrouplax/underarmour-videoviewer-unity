# README #

Here are the steps to get the Video Viewer up and running.  It was developed to run on Windows 10 touch screen device like the OLED. 

### What is this repository for? ###

* Version 1.1
* This is a touch screen application that allows you to find out additional information about under armour sleepwear and footwear. However,  this project was built as a product so it should be easily customizable.
* Functional pieces:  
1 - Time out and return to attract
2 - horizontal swipe page view controller
3 - Hot spots and menu items navigation


### How do I get set up? ###

* Summary of set up: load the exe and its associated data folder then launch the EXE
* Configuration - 1080 X 1920 resolution
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review - good modular code
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin - Darryl Hill/SpinifexGroup - sirdarrylhill@gmail.com
* Other community or team contact